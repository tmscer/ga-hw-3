const express = require('express');
const bodyParser = require('body-parser');
const port = 8000;

const app = express();
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// f0 = 0, f1 = 1
// fn = fn-1 + fn-2
// >> 0, 1, 1, 2, 3, 5, 8, 13, ...
function fibo(n) {
    let a = 1; // fi
    let b = 0; // fi-1
    for (let i = 0; i < n; i++) {
        let tmp = a;
        a = a + b;
        b = tmp;
    }
    return b;
}

app.get('/:n', (req, res) => {
    let n = Number(req.params.n);
    // n must be >= 0
    if (isNaN(n) || n < 0) {
        res.send(400);
        return;
    }
    // return fn
    res.send(`${fibo(n)}`);
});

app.get('/', (req, res) => {
    res.send(
        '<form method="POSt">' +
        'First num:<br>' +
        '<input type="number" name="a"><br>' +
        'Second num:<br>' +
        '<input type="number" name="b">' +
        '<input type="submit" value="Sum">' +
        '</form> '
    );
});

app.post('/', (req, res) => {
    let a = Number(req.body.a);
    let b = Number(req.body.b);
    if (isNaN(a) || isNaN(b)) {
        res.send(400);
        return;
    }
    res.send(`The sum is ${a + b}. <a href="/">Sum something else.</a>`);
});

app.listen(port, () => {
    console.log(`Express app is listening on port ${port}.`);
});

